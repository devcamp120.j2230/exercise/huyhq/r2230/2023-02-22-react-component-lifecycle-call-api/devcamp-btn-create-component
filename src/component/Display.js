import { Component } from "react";

class Display extends Component{
    constructor(props){
        super(props);
    };

    componentWillMount = ()=>{
        console.log('Component Wiil Mount');
    };

    componentDidMount = ()=>{
        console.log('Component Did Mount');
    };

    shouldComponentUpdate = ()=>{
        console.log('Should Component Update');
        return true;
    };

    componentWillUpdate = ()=>{
        console.log('Component Will update');
    };

    componentDidUpdate = ()=>{
        console.log("Component Did Update");
    };

    componentWillUnmount = ()=>{
        console.log("Component Will Unmount");
    };

    render(){
        return(
            <h1>I exist !</h1>
        )
    }
}

export default Display;