import { Component } from "react";
import Display from "./Display";

class Parent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            display: false
        };
    };

    updateDisplay = () => {
        this.setState({
            display: !this.state.display ? true : false,
        })
    }

    render() {
        return (
            <div>
                {!this.state.display
                    ? <button onClick={this.updateDisplay}>Create Component</button>
                    : <button onClick={this.updateDisplay}>Destroy Component</button>
                }
                {this.state.display ? <Display display={this.state.display} /> : null}
            </div>
        )
    }
}

export default Parent;